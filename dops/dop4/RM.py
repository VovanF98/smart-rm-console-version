def fib(i):
    fib1 = 1
    fib2 = 1
    if i == 1:
        return 1
    if i == 2:
        return 1
    fibn = 0
    for x in range(i - 2):
        fibn = fib1+fib2
        fib1 = fib2
        fib2 = fibn
    return fibn


genfib = (fib(val) for val in range(100))

for i in genfib:
    print(i)