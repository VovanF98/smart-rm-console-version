import re

container = set()

def print_menu():
    print("Please, enter the the action, you want to perform")
    print("Add <Key>/[<Key>, ...]")
    print("Remove <Key>/[<Key>, ...]")
    print("Find <Key>/[<Key> ...]")
    print("List")
    print("grep <regexp>")
    print("Save")
    print("Load")
    print("To exit thr program print exit()")

def list_():
    for i in container:
        print(str(i))

def add(list_of_keys):
    for i in list_of_keys:
        container.add(i)

def remove(list_of_keys):
    for i in list_of_keys:
        container.remove(i)

def find(list_of_keys):
    for i in list_of_keys:
        if i in container:
            print(i)

def save():
    file = open("C:/users/vova/desktop/file_for_lab.txt", "w")
    for i in container:
        file.write(i)
        file.write('\n')
    file.close()

def load():
    file = open("C:/users/vova/desktop/file_for_lab.txt", "r")
    if(len(container)):
        print("The info, stored in current container will be lost!!! Are you sure you want to continue?[y/n]")
        ans = input('>>>')
        if ans == 'y':
            container.clear()
            for line in file:
                line = line.strip('\n')
                container.add(line)
        file.close()
        return
    for line in file:
        line = line.strip('\n')
        container.add(line)
    file.close()

def make_string():
    reg_exp_str = ''
    for i in container:
        reg_exp_str += str(i) + ' '
    return reg_exp_str

def parse_input(s):
    parsed_list = s.split()
    if (parsed_list[0] == 'Add') | (parsed_list[0] == 'add'):
        add(parsed_list[1:])
    if (parsed_list[0] == 'Remove') | (parsed_list[0] == 'remove'):
        remove(parsed_list[1:])
    if (parsed_list[0] == 'Find') | (parsed_list[0] == 'find'):
        find(parsed_list[1:])
    if (parsed_list[0] == 'List') | (parsed_list[0] == 'list'):
        list_()
    if (parsed_list[0] == 'Save') | (parsed_list[0] == 'save'):
        save()
    if (parsed_list[0] == 'Load') | (parsed_list[0] == 'load'):
        load()
    if parsed_list[0] == 'grep':
        if len(parsed_list) == 2:
            list_of_matches = re.findall(parsed_list[1], make_string())
            print(str(list_of_matches) + "\n")
        else:
            print("Incorrect regular expression!\n")




s = 'Start the program'
while(s != 'exit()'):
    print_menu()
    s = input('>>>')
    parse_input(s)









