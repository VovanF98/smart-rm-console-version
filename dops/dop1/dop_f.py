import re
import numpy as np
file = open("C:/users/vova/Desktop/text.txt", "r", encoding = "utf - 8 - sig");

a = []
words_dict = {}
sentences_list = []

for line in file:
    line = line.strip('\n')
    sentence_to_process = re.split(r'[!.?/\n]', line)
    for i in sentence_to_process:
        if i != '':
            sentences_list.append(i)
    words_list = re.findall(r'\w+', line)
    for i in words_list:
       words_dict[i] = words_dict.get(i, 0) + 1

for i in words_dict:
    print(i + " : " + str(words_dict[i]))

amount_of_words = 0
number_of_words_in_sentence = []

for i in sentences_list:
    amount_of_words += len(re.split(r'\w+', i))
    number_of_words_in_sentence.append(len(re.split(r'\w+', i)))

print("The median amount of words each sentence is: " + str(np.median(number_of_words_in_sentence)) + " words")
print("The average amount of words per sentence is: " + str(amount_of_words/len(sentences_list)) + " words")

file.close()
