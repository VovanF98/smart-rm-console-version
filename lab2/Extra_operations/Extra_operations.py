import os


def dir_size(path):
    """
    Function that gets the current size of chosen directory/file
    """
    total_size = 0
    if not os.path.isdir(path):
        return os.path.getsize(path)
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)

    return total_size
