#!/usr/bin/env python2.7
from setuptools import setup, find_packages

setup(
    name='RmF98',
    version='0.0.1',
    packages=find_packages(exclude=['srm.test']),
    entry_points={
        'console_scripts': {
            'SrmF98 = Removing_operations.SrmF98:main',
            'Trash = Trash_operations.Trash:main',
            'Config = Config.Config:main'
        }
    },
    description='Program for removing/restoring your files',
    author='Uladzimir Tumanau',

)
