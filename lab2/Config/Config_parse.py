import argparse


def start():

    parser = argparse.ArgumentParser(description='Process Config file')
    parser.add_argument("-p", "--path", nargs="?", help="Path to the trash")
    parser.add_argument("-cp", "--clean_pol", nargs="?", help="Chooses policy for cleaning trash")
    parser.add_argument("-rp", "--restore_pol", nargs="?", help="Chooses policy for restoring file")
    parser.add_argument("-am", "--amount", nargs="?", help="Determines the amount of files to be deleted by any policy"
                                                           " while auto cleaning")
    parser.add_argument("-ms", "--max_size", nargs="?", help="Determines the max size of the trash")
    parser.add_argument("-i", "--inter", action="store_true", help="Turns on interactive mode")

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    start()
