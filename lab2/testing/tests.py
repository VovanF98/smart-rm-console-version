import os
import unittest
import tempfile
import shutil
from Config.Config import Config_file
from Trash_operations.Trash import Trash
from Removing_operations.SrmF98 import SrmHandler
from Checker import Checker


class Test(unittest.TestCase):

    def setUp(self):
        trash_path = tempfile.mkdtemp()
        self.conf_file = Config_file()
        self.conf_file_info = self.conf_file.get_config_info()
        self.conf_file_info["path_to_trash"] = trash_path
        self.trash = Trash(self.conf_file_info, False, True)
        self.smrm = SrmHandler(self.conf_file_info, False, True)

    def test_make_dir(self):
        test_path = self.trash.path_to_trash
        Checker.make_dir(test_path)
        self.assertTrue(os.path.exists(test_path))

    def test_remove(self):

        file_1 = tempfile.NamedTemporaryFile(delete=False)
        file_2 = tempfile.NamedTemporaryFile(delete=False)

        self.smrm.add_to_trash(self.smrm.form_remove_list([file_1.name, file_2.name]))

        self.assertFalse(os.path.exists(file_1.name), 'File remove is not working')
        self.assertFalse(os.path.exists(file_2.name), 'File remove is not working')

    def test_rmdir(self):
        dir_path_1 = tempfile.mkdtemp()
        dir_path_2 = tempfile.mkdtemp()

        self.smrm.add_to_trash(self.smrm.form_remove_list([dir_path_1, dir_path_2]))

        self.assertFalse(os.path.exists(dir_path_1), 'Directory remove is not working')
        self.assertFalse(os.path.exists(dir_path_2), 'Directory remove is not working')

    def test_remove_by_regexp(self):
        dir_path = tempfile.mkdtemp()
        sub_dir_path = tempfile.mkdtemp(dir=dir_path)

        file_1 = tempfile.NamedTemporaryFile(prefix='Test', dir=dir_path, delete=False)
        file_2 = tempfile.NamedTemporaryFile(prefix='False', dir=dir_path, delete=False)
        file_3 = tempfile.NamedTemporaryFile(prefix='Test', dir=sub_dir_path, delete=False)

        self.smrm.add_to_trash( self.smrm.remove_regexp(dir_path, 'Test.*'))

        self.assertFalse(os.path.exists(file_1.name), 'The file was not removed')
        self.assertFalse(os.path.exists(file_3.name), 'The file was not removed')
        self.assertTrue(os.path.exists(file_2.name), 'The wrong file was removed')

    def test_remove_and_restore(self):
        dir_path = tempfile.mkdtemp()
        file_in_dir_1 = tempfile.NamedTemporaryFile(dir=dir_path, delete=False)
        file_in_dir_2 = tempfile.NamedTemporaryFile(dir=dir_path, delete=False)

        self.smrm.add_to_trash(self.smrm.form_remove_list([dir_path]))
        self.assertFalse(os.path.exists(file_in_dir_1.name), 'The file was not removed')
        self.assertFalse(os.path.exists(file_in_dir_2.name), 'The file was not removed')

        self.trash.restore([dir_path])
        self.assertFalse(os.path.exists(self.trash.files + file_in_dir_1.name),
                         'The file was not restored')
        self.assertFalse(os.path.exists(self.trash.files + file_in_dir_2.name),
                         'The file was not restored')

    def test_clear_trash(self):
        file_1 = tempfile.NamedTemporaryFile(delete=False)
        file_2 = tempfile.NamedTemporaryFile(delete=False)

        self.smrm.add_to_trash(self.smrm.form_remove_list([file_1.name, file_2.name]))

        self.trash.clear_trash()

        self.assertTrue(len(os.listdir(self.trash.files)) == 0)

    def test_remove_file_from_trash(self):
        file_1 = tempfile.NamedTemporaryFile(delete=False)

        self.smrm.add_to_trash(self.smrm.form_remove_list([file_1.name]))

        self.trash.remove_item_from_trash([os.path.basename(file_1.name)])

        self.assertTrue(len(os.listdir(self.trash.info)) == 0)

    def tearDown(self):
        shutil.rmtree(self.trash.path_to_trash)

if __name__ == '__main__':
    unittest.main()