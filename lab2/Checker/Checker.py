import os
import logging
from Policies import Policy_clean
import Asker.Asker as Asker


def make_dir(path):
    try:
        os.mkdir(path)
    except OSError:
        pass


def from_list_to_string(item):
    if isinstance(item, list):
        return item[0]
    else:
        return item


def check_size(name, cur_size_of_trash, max_size_of_trash, cleaning_policy, data, force, deny, inter):
    if int(check_size_of_d(name)) + int(cur_size_of_trash) > int(max_size_of_trash):
        pc = Policy_clean.Policy_clean(data)
        if cleaning_policy == "by_size":
            list_autodelete = pc.by_size()
        else:
            list_autodelete = pc.by_date()
        if not force and inter == "True":
            answer = Asker.ask_to_auto_delete(cleaning_policy)
            print "Files, suggested by the auto deletion policy: ", list_autodelete
            if answer:
                pc.autoclean(list_autodelete)
            else:
                deny = True
        else:
            pc.autoclean(list_autodelete)
        return False
    else:
        return True


def inside_trash(trash, path):
    return str(path).find(str(trash)) != -1


def outside_trash(trash, path):
    return str(path).find(str(trash)) == -1


def check_file(file_name):
    try:
        f = open(str(file_name), "r")
    except IOError:
        f = open(str(file_name), "w")
        f.close()
        f = open(str(file_name), "r")
    return f


def create_logging():
    try:
        logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s',
                            level=logging.INFO, filename=u'/home/vovanf98/Desktop/lab2_main/mylog.log')
    except IOError:
        f = open('/home/vovanf98/Desktop/lab_2/lab2_main/mylog.log', "w")
        f.close()
        logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s',
                            level=logging.INFO, filename=u'/home/vovanf98/Desktop/lab2_main/mylog.log')

def check_size_of_d(path):
    total_size = 0
    if not os.path.isdir(path):
        return os.path.getsize(path)
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size



