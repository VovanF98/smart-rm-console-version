import os
import re
from Checker import Checker
from Extra_operations import Extra_operations
import Srm_Parse
import logging
import multiprocessing
import datetime as dt
import Config.Config as Config
from Asker import Asker as Asker


def help(arg, **kwarg):
    return SrmHandler.add_to_trash(*arg, **kwarg)


class SrmHandler:

    def __init__(self, data, name, dryrun, silent, force):
        if os.path.exists(Checker.from_list_to_string(data["path_to_trash"])):
            self.path_to_trash = Checker.from_list_to_string(data["path_to_trash"])
        else:
            self.path_to_trash = os.path.join(os.path.expanduser("~"), "Desktop")
        Checker.create_logging()
        self.data = data
        self.info = os.path.join(self.path_to_trash, "info")
        self.files = os.path.join(self.path_to_trash, "files")
        self.list_to_remove = []
        self.dry_run = dryrun
        self.silent = silent
        self.force = force
        self.clean_pol = Checker.from_list_to_string(data["cleaning_policy"])
        self.cur_size_of_trash = 0
        self.max_size_of_trash = Checker.from_list_to_string(data["size_of_trash"])
        if "inter" in data.keys():
            self.inter = Checker.from_list_to_string(data["inter"])
        else:
            self.inter = "False"

    def add_to_trash(self, lst_to_remove):
        """
        The main function, that performs deleting the given list of files to trash
        """
        cnt = 0
        if not isinstance(lst_to_remove, list):
            list_to_remove = []
            list_to_remove.append(lst_to_remove)
        else:
            list_to_remove = lst_to_remove
        for name in list_to_remove:
            print name
            if not self.force and self.inter == "True":
                if not Asker.ask(name):
                        continue
            try:
                if not self.dry_run:
                    cnt += 1
                    copy_number = 1
                    trial_name = os.path.basename(name)
                    while os.path.lexists(os.path.join(self.info, trial_name)):
                        trial_name = os.path.basename(name)
                        str_copy_number = '_{0}'.format(copy_number)
                        trial_name += str_copy_number
                        copy_number += 1
                    if int(self.max_size_of_trash) < int(Checker.check_size_of_d(name)):
                        print "File is too big. Please check the max size of your trash\n"
                        continue
                    ok = False
                    deny = False
                    while not ok:
                        ok = Checker.check_size(name, Extra_operations.dir_size(self.files),
                                                self.max_size_of_trash, self.clean_pol, self.data, self.force, deny, self.inter)
                        if deny:
                            return
                    os.rename(name, os.path.join(self.files, trial_name))
                    with open(os.path.join(self.info, trial_name), 'w') as file_:
                        file_.write(name + '\n' + str(dt.datetime.now()))
                if not self.silent:
                    print "Object {0} successfully deleted\n".format(name)
                    logging.info("Object {0} successfully deleted\n".format(name))
            except:
                if not self.silent:
                    print "Error while deleting file {0}\n".format(name)
                logging.error('Error while restoring file %s' % name)

    def remove_regexp(self, init_path, regexp):
        """
        Function, that returns the list of items that match regular expression
        """
        init_path = str(init_path).encode('ascii', 'ignore')
        regexp = str(regexp).encode('ascii', 'ignore')
        if not os.path.isdir(str(init_path)):
            if re.search(regexp, str(init_path)):
                self.list_to_remove.append(init_path)
        for path in os.listdir(str(init_path)):
            if Checker.outside_trash(str(self.path_to_trash).encode('ascii', 'ignore'),
                                     str(os.path.join(init_path, path)).encode('ascii', 'ignore')) and path[0] != '.':
                if re.search(regexp, path):
                    self.list_to_remove.append(os.path.join(str(init_path), str(path)))
                else:
                    path = os.path.join(str(init_path), str(path))
                    if os.path.isdir(str(path)):
                        self.remove_regexp(path, regexp)
        return self.list_to_remove

    def multi_delete_to_trash(self, files, data, config_data):
        """
        Function that organizes the multiprocessing deleting of file
        """
        def Generate():
            for name in files:
                yield name
        pool = multiprocessing.Pool(8)
        for res in pool.imap(help, zip([self] * len(files), Generate()), 1):
            pass
        pool.close()
        pool.join()

    def form_remove_list(self, list_remove):
        """
        Forms list of files that do exist
        """
        for path in list_remove:
            if os.path.exists(path):
                if not re.match(str(self.path_to_trash), path):
                    self.list_to_remove.append(path)
        return self.list_to_remove


def main():
    args = Srm_Parse.start()
    config_file = Config.Config_file()
    data = config_file.get_config_info()
    srm = SrmHandler(data, "abc", args.dryrun, args.silent, args.force)
    if args.regexp:
        if len(args.regexp) == 1:
            srm.add_to_trash(srm.remove_regexp(os.getcwd(), args.regexp[0]))
        else:
            srm.add_to_trash(srm.remove_regexp(args.regexp[0], args.regexp[1]))
    else:
        rem_list = srm.form_remove_list(args.remove)
        srm.add_to_trash(rem_list)

if __name__ == '__main__':
    main()
