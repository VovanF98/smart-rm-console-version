import argparse
import os


def start():
    parser = argparse.ArgumentParser(description='Process some files')
    parser.add_argument("-r", "--remove", nargs="+", help="Remove some files and directories, listed in arguments")
    parser.add_argument("-re", "--regexp", nargs="+", help="Delete by some regular expression")
    parser.add_argument("-d", "--dryrun", action="store_true", help="Activate dryrun mode")
    parser.add_argument("-s", "--silent", action="store_true", help="Activate silent mode")
    parser.add_argument("-f", "--force", action="store_true", help="Activate force mode")

    args = parser.parse_args()

    if args.remove:
        cnt = 0
        for name in args.remove:
            if not os.path.exists(name):
                args.remove[cnt] = os.path.abspath(name)
            else:
                args.remove[cnt] = os.path.join(os.getcwd(), name)
            cnt += 1

    return args

if __name__ == '__main__':
    start()

