import os
from Extra_operations import Extra_operations
import shutil


class Politics_clean:

    def __init__(self, data):
        self.data = data
        self.path_to_trash = Politics_clean.from_list_to_string(self, data["path_to_trash"])
        self.path_to_info = os.path.join(self.path_to_trash, "info")
        self.path_to_file = os.path.join(self.path_to_trash, "files")
        self.list_of_files = os.listdir(self.path_to_file)
        self.list_of_info = os.listdir(self.path_to_info)
        self.list_of_candidates = []
        self.auto_delete_anount = Politics_clean.from_list_to_string(self, self.data["number_of_objects_to_auto_delete"])

    def by_size(self):
        """
        Function that performs cleaning politic by size
        """
        for name in self.list_of_files:
            self.list_of_candidates.append([Extra_operations.dir_size(os.path.join(self.path_to_file, name)), name])
        self.list_of_candidates.sort()
        self.list_of_candidates.reverse()
        list_of_candidates = self.list_of_candidates[:min(len(self.list_of_candidates),
                                                          int(self.auto_delete_anount))]
        return list_of_candidates

    def by_date(self):
        """
        Function that performs cleaning politic by date
        """
        for name in self.list_of_info:
            self.list_of_candidates.append([os.stat(os.path.join(self.path_to_info, name))[8], name])
        self.list_of_candidates.sort()
        self.list_of_candidates.reverse()
        list_of_candidates = self.list_of_candidates[:min(len(self.list_of_candidates),
                                                     int(self.auto_delete_anount))]
        return list_of_candidates

    def autoclean(self, list_to_delete):
        """
        Function that performs deleting files from trash. Can not be undone
        """
        for name in list_to_delete:
            if isinstance(name, list):
                name = name[1]
            if os.path.isfile(os.path.join(self.path_to_file, name)):
                os.remove(os.path.join(self.path_to_file, name))
            else:
                shutil.rmtree(os.path.join(self.path_to_file, name))
            os.remove(os.path.join(self.path_to_info, name))

    def from_list_to_string(self, item):
        if isinstance(item, list):
            return item[0]
        else:
            return item


def main():
    pass

if __name__ == '__main__':
    main()


