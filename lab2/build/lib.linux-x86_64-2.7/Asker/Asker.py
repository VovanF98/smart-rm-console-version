import os


def ask(name):
    if os.path.isdir(name):
        print "Are you sure you want to delete directory {0}?(Y/N)".format(name)
    else:
        print "Are you sure you want to delete file {0}?(Y/N)".format(name)
    s = raw_input()
    if s == 'Y':
        return True
    else:
        return False


def ask_to_auto_delete(cleaning_policy):
    print "The trash is full. We recommend you to delete objects, that are chosen by {0} policy" \
          "or you can perform it manually. Would you like to auto delete chosen files?" \
          "(Y/N)".format(cleaning_policy)
    s = raw_input()
    return s == 'Y'


def ask_to_remove(name):
    print "Are you sure you want to remove {0} from trash. This action can not be undone!(Y/N)".format(name)
    s = raw_input()
    return s == 'Y'


def ask_to_remove_all():
    print "Are you sure you want do delete all objects from trash? This action can not be undone!(Y/N)"
    s = raw_input()
    return s == 'Y'


def ask_to_clear_trash():
    print "Are you sure you want to clear trash?(Y/N)"
    s = raw_input()
    return s == 'Y'

