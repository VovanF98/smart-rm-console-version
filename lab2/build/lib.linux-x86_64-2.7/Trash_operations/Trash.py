import os
import shutil
from Checker import Checker as Checker
from Extra_operations import Extra_operations
import Trash_Parse
import logging
import Config.Config as Config
from Asker import Asker
import multiprocessing


def help(arg, **kwarg):
    print "In help"
    return Trash.remove_item_from_trash(*arg, **kwarg)


def help2(arg, **kwarg):
    print "In help2"
    return Trash.restore(*arg, **kwarg)


class Trash:

    def __init__(self, data, name, dryrun, silent, force):
        if os.path.exists(Checker.from_list_to_string(data["path_to_trash"])):
            self.path_to_trash = str(Checker.from_list_to_string(data["path_to_trash"])).encode("ascii", "ignore")
        else:
            self.path_to_trash = os.path.join(os.path.expanduser("~"), "Desktop")
        self.info = os.path.join(self.path_to_trash, "info")
        self.files = os.path.join(self.path_to_trash, "files")
        Checker.make_dir(self.path_to_trash)
        Checker.make_dir(self.info)
        Checker.make_dir(self.files)
        Checker.create_logging()
        self.amount = Checker.from_list_to_string(data["number_of_objects_to_auto_delete"])
        self.dry_run = dryrun
        self.silent = silent
        self.force = force
        self.rest_pol = Checker.from_list_to_string(data["restoring_policy"])
        if "inter" in data.keys():
            self.inter = Checker.from_list_to_string(data["inter"])
        else:
            self.inter = "False"
        self.path_to_cfg = os.path.join(self.path_to_trash, name) + ".cfg"

    def multi_recover_from_trash(self, files, data, config_data):
        '''
        Function which enables to perform multiprocessing in file restoring
        '''
        def Generate():
            for name in files:
                yield name
        pool = multiprocessing.Pool(8)
        for res in pool.imap(help2, zip([self] * len(files), Generate()), 1):
           pass
        pool.close()
        pool.join()

    def multi_delete_from_trash(self, files, data, config_data):
        '''
        Function which enables to perform multiprocessing in file removing
        '''

        def Generate():
            for name in files:
                print "generating {0}".format(name)
                yield name
        pool = multiprocessing.Pool(8)
        for res in pool.imap(help, zip([self] * len(files), Generate()), 1):
            pass
        pool.close()
        pool.join()

    def restore_all(self):
        """
        Function that allows to restore all files from trash
        """
        list_to_restore = []
        if len(os.listdir(self.files)):
            for name in os.listdir(self.files):
                list_to_restore.append(name)
        self.restore(list_to_restore)

    def restore(self, lst_to_restore):
        """
        Function that restores the chose files from current trash
        """
        if not isinstance(lst_to_restore, list):
            list_to_restore = []
            list_to_restore.append(os.path.basename(lst_to_restore))
        else:
            list_to_restore = lst_to_restore
        for name in list_to_restore:
            if os.path.exists(os.path.join(self.files, name)):
                try:
                    with open(os.path.join(self.info, name), 'r') as file_:
                        rest_path = file_.readline()[:-1]
                        for item in os.listdir(os.path.dirname(rest_path)):
                            if item == os.path.basename(rest_path) and self.rest_pol == "rename":
                                print "There is a file with name {0} in directory. Please, enter " \
                                      "new file name: ".format(name)
                                s = raw_input()
                                rest_path = os.path.join(os.path.dirname(rest_path), s)
                                break
                        try:
                            if not self.dry_run:
                                os.rename(os.path.join(self.files, name), rest_path)
                                os.remove(os.path.join(self.info, name))
                            logging.info('Object %s successfully restored to path %s', name, rest_path)
                            if not self.silent:
                                print "Object {0} successfully restored to path {1}\n".format(name, rest_path)
                        except:
                            logging.error('Error while restoring file %s' % name)
                            if not self.silent:
                                print "Error while restoring file {0} \n".format(name)
                except:
                    if not self.silent:
                        print "Error while restoring file {0} \n".format(name)
                    logging.error('Error while restoring file %s' % name)

    def clear_trash(self):
        """
        Function was made to clear the trash if the size of trash does not allow to placesome more files in it
        """
        if not self.force and self.inter == "True":
            if not Asker.ask_to_clear_trash():
                return
        if not self.dry_run:
            shutil.rmtree(self.path_to_trash)
            Checker.make_dir(self.path_to_trash)
            Checker.make_dir(self.info)
            Checker.make_dir(self.files)
        if not self.silent:
            print "Trash has been successfully cleared\n"
        logging.info('Trash has been successfully cleared')

    def remove_item_from_trash(self, items):
        """
        Function that allows the user to remove chosen items from trash. Can not be undone
        """
        if not isinstance(items, list):
            all_items = []
            all_items.append(os.path.basename(items))
        else:
            all_items = items
        for item in all_items:
            full_path = os.path.join(self.files, item)
            full_path2 = os.path.join(self.info, item)
            if os.path.exists(full_path) and not self.dry_run:
                if self.inter:
                    if Asker.ask_to_remove(item):
                        if not os.path.isdir(full_path):
                            os.remove(full_path)
                        else:
                            shutil.rmtree(full_path)
                        os.remove(full_path2)
                else:
                    if not os.path.isdir(full_path):
                        os.remove(full_path)
                    else:
                        shutil.rmtree(full_path)
                    os.remove(full_path2)

    def list(self, amount):
        """
        Function that lists all existing files in current trash
        """
        count = 0
        if not self.dry_run:
            for filename in os.listdir(self.files):
                count += 1
                print "\n{0}). {1} ----- {2}bytes".format(str(count), filename,
                                                          Extra_operations.dir_size(os.path.join(self.files, filename)))
                if count == amount:
                    return
        if not self.silent:
            print "Trash has been successfully listed\n"
        logging.info('Trash has been successfully listed')


def main():
    config_file = Config.Config_file()
    data = config_file.get_config_info()
    args = Trash_Parse.start()
    trash = Trash(data, "abc", args.dryrun, args.silent, args.force)
    if args.restall:
        trash.restore_all()
    if args.restore:
        trash.restore(args.restore)
    if args.list:
        trash.list(args.list)
    if args.clear:
        trash.clear_trash()

if __name__ == '__main__':
    main()


