import Config_parse
import json
import os


class Config_file:

    def __init__(self, path_to_cfg=None):
        if path_to_cfg is None:
            self.path_to_config = '/home/vovanf98/Desktop/lab2_main/lab2/Config/Config.json'
        else:
            self.path_to_config = path_to_cfg

    def make_config_file(self, path_to_trash=None, cleaning_policy=None,
                         size_of_trash=None, am_to_delete=None,
                         restoring_policy=None, inter=None, cur_size=None):
        """
        Function creates new config file in which all parameters are placed. If some of the parameters are not given
        it creates them by default. The two config files are created: First one in json format, second in human-readable
        format
        """

        if not path_to_trash or not os.path.exists(os.path.dirname(path_to_trash)):
            path_to_trash = '/home/vovanf98/Desktop/lab2_main/lab2/trash'
        if restoring_policy not in ['substitute', 'rename']:
            restoring_policy = 'substitute'
        if cleaning_policy not in ['by_size', 'by_date']:
            cleaning_policy = 'by_size'
        if not str(size_of_trash).isdigit():
            size_of_trash = 10000
        if not str(am_to_delete).isdigit():
            am_to_delete = 20
        if not inter:
            inter = False
        if not cur_size:
            cur_size = 0
        with open(self.path_to_config, 'w') as conf_json:
            conf_json.write(json.dumps({'path_to_trash': str(path_to_trash),
                                    'size_of_trash': str(size_of_trash),
                                    'restoring_policy': str(restoring_policy),
                                    'cleaning_policy': str(cleaning_policy),
                                    'number_of_objects_to_auto_delete': str(am_to_delete),
                                    'inter': str(inter),
                                    'cur_size': str(cur_size)}))
        with open('/home/vovanf98/Desktop/lab2_main/lab2/Config/Config.readable', 'w') as conf_readable:
            conf_readable.writelines('path to trash is :' + '\t\n' + str(path_to_trash) + '\n')
            conf_readable.writelines('Amount of objects to delete while auto cleaning :' + '\n\t' +
                                     str(am_to_delete) + '\n\n')
            conf_readable.writelines('Restoring policy :' + '\n\t' + str(restoring_policy) + '\n\n')
            conf_readable.writelines('Cleaning policy :' + '\n\t' + str(cleaning_policy) + '\n\n')
            conf_readable.writelines('Max size trash :' + '\n\t' + str(size_of_trash) + '\n\n')
            conf_readable.writelines('Interactive mode :' + '\n\t' + str(inter) + '\n\n')
            conf_readable.writelines('Current size :' + '\n\t' + str(cur_size) + '\n\n')

    def get_config_info(self):
        """
        Function that allows to get all config parameters in json format and convert them into dict
        """
        with open(self.path_to_config) as data_file:
            data = json.load(data_file)
        return data


def main():

    args = Config_parse.start()
    print "Started parsing Config"
    config_file = Config_file()
    config_file.make_config_file(args.path, args.clean_pol, args.max_size, args.amount,
                                 args.restore_pol, args.inter)


if __name__ == '__main__':
    main()

