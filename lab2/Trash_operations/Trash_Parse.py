import argparse


def start():

    parser = argparse.ArgumentParser(description='Process trash')
    parser.add_argument("-ri", "--remove_item", nargs="+", help="Remove the following item from the trash")
    parser.add_argument("-l", "--list", type=int, help="Show the list of items in our bucket")
    parser.add_argument("-c", "--clear", action="store_true", help="clears the bucket")
    parser.add_argument("-rest", "--restore", nargs="+", help="Restore the given list of files")
    parser.add_argument("-d", "--dryrun", action="store_true", help="Activate dryrun mode")
    parser.add_argument("-s", "--silent", action="store_true", help="Activate silent mode")
    parser.add_argument("-resta", "--restall", action="store_true", help="Restore all elements in trash")
    parser.add_argument("-f", "--force", action="store_true", help="Activate force mode")

    args = parser.parse_args()

    return args

if __name__ == '__main__':
    start()

